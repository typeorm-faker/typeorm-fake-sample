import { DataSource } from "typeorm";
import { Post } from "../../entities/posts.entity";

export class PostsService {

    datasource!: DataSource;

    async fetchPosts(): Promise<Post[]> {


        const postRepository = this.getDatasource().getRepository(Post);

        const posts = await postRepository.find({
            order: {
                id: 'DESC'
            },
            take: 10
        });

        return posts;
    }

    getDatasource() {
        if (!this.datasource) {
            this.datasource = new DataSource({} as any);
        }

        return this.datasource;
    }
}
