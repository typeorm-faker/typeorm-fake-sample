import { expect } from "chai";
import { describe, it } from "mocha";
import { PostsService } from "./posts.service";

import sinon from 'sinon';
import { DataSource, Repository } from "typeorm";
import { Post } from "../../entities/posts.entity";

describe('Test Post service', () => {

    let service: PostsService = new PostsService();

    let repositoryStub: sinon.SinonStubbedInstance<Repository<Post>>

    const postStubs: Post[] = stub(Post);

    // you can also use like this with global variable typeormFaker or direct import.
    // const _postStubs: Post[] = typeormFaker.stub(Post);

    const firstPostStub: Post = stubOne(Post);

    before(() => {

        repositoryStub = sinon.createStubInstance(Repository);
        repositoryStub.find.resolves([
            firstPostStub,
            ...postStubs
        ])

        const datasourceStub = sinon.createStubInstance(DataSource);
        datasourceStub.getRepository.returns(repositoryStub);

        sinon.stub(service, 'getDatasource').returns(datasourceStub);
    })

    after(() => {
        sinon.restore();
    })

    it('should initialize service without error', () => {
        expect(service).ok;
    })

    it('should fetch posts', async () => {

        const posts = await service.fetchPosts();

        expect(posts).ok;
        expect(posts.length).greaterThan(0);

        const firstPost = posts[0];
        const secondPost = posts[1];

        /**
         * firstPost: Post {
         *     id: 13326,
         *     title: 'pDYFMFO,ZX',
         *     text: 'eW2`p3tj*A',
         *     likesCount: 0
         * }
         */
        console.log('firstPost:', firstPost);

        expect(firstPost).deep.equal(firstPostStub);
        expect(secondPost).deep.equal(postStubs[0]);
    })

    it('should fetch post stub raw one', () => {

        const rawStub = stubRawOne<Post, 'post', 'field1' | 'field2'>(Post, {
            field1: 'custom field 1',
            field2: 'custom field 2',
            post_text: 'Typeorm Entity name + underbar + property'
        });

        /**
         * rawStub: {
         *     post_id: 31558,
         *     post_title: "ufwS5'{(aH",
         *     post_text: 'Typeorm Entity name + underbar + property',
         *     post_likes_count: 0,
         *     field1: 'custom field 1',
         *     field2: 'custom field 2'
         * }
         */
        console.log('rawStub:', rawStub);

        expect(rawStub);
        expect(rawStub.field1).ok;
        expect(rawStub.field2).ok;
        expect(rawStub.post_text).ok;
    });

    it('should fetch post stub raws', () => {

        const rawStubs = stubRaw<Post, 'post', 'field1' | 'field2'>(Post, 3, {
            field1: 'custom field 1',
            field2: 'custom field 2',
            post_text: 'Typeorm Entity name + underbar + property'
        });

        /**
         * rawStubs:
         * [
         *     {
         *         post_id: 20796,
         *         post_title: "sR(mX=Vo9'",
         *         post_text: 'Typeorm Entity name + underbar + property',
         *         post_likes_count: 0,
         *         field1: 'custom field 1',
         *         field2: 'custom field 2'
         *     },
         *     {
         *         post_id: 27938,
         *         post_title: '}qgJ;V0BNC',
         *         post_text: 'Typeorm Entity name + underbar + property',
         *         post_likes_count: 0,
         *         field1: 'custom field 1',
         *         field2: 'custom field 2'
         *     },
         *     {
         *         post_id: 37090,
         *         post_title: 'HG1C_[o=bW',
         *         post_text: 'Typeorm Entity name + underbar + property',
         *         post_likes_count: 0,
         *         field1: 'custom field 1',
         *         field2: 'custom field 2'
         *     }
         * ]
         */
        console.log(rawStubs);

        expect(rawStubs.length).greaterThan(0);
    });

    it('should fetch post stub raw one without additional fields', () => {

        const rawStub = typeormFaker.stubRawOne<Post, 'post'>(Post, {
            post_text: 'Typeorm Entity name + underbar + property'
        });

        /**
         * rawStub: {
         *     post_id: 31558,
         *     post_title: "ufwS5'{(aH",
         *     post_text: 'Typeorm Entity name + underbar + property',
         *     post_likes_count: 0
         * }
         */
        console.log('rawStub:', rawStub);

        expect(rawStub);
        expect(rawStub.post_text).ok;
    });

    it('should be patched post stub address as null without missing by class-transformer', () => {

        const stub = stubOne(Post, {
            title: 'ShowMeTheJob',
            address: null
        }, {
            excludeExtraneousValues: false
        });

        expect(stub).ok;
        expect(stub.address).null;
    });
});
