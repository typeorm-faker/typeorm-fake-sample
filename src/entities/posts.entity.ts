import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'
import { Expose } from 'class-transformer';

@Entity('post')
export class Post {
    @PrimaryGeneratedColumn()
    id!: number

    @Column()
    title!: string

    @Column()
    text?: string

    @Column({
        nullable: false,
        default: 0
    })
    likesCount!: number

    @Column()
    @Expose({ name: '_address'})
    address!: string | null
}